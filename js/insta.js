function showSearchInputField(event) {
  const button = event.target;
  button.classList.add('hide');
  button.nextElementSibling.classList.remove('hide');
}